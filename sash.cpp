#include "stdio.h"
#include "iostream"
#include "omp.h"
#include <ctime>
#include "clocale"
#include <time.h>
using namespace std;

int main()//Test 34
{
	setlocale(LC_CTYPE, "rus");

	int size_array = 100000000;
	double *array_1 = new double [size_array];
	double *array_2 = new double [size_array];

	#pragma omp parallel for num_threads(3)
		for(int i = 0; i < size_array; i++)
		{
			array_1[i] = 1.0;
			array_2[i] = 1.0;
		}
	

	double sum = 0;
	unsigned long start_time = clock();

	for (int i = 0; i < size_array; i++) {
		sum += array_1[i] * array_1[i];
	}

	unsigned long end_time = clock();
	unsigned long summ_time = end_time - start_time;

	printf("\nВремя работы: %f сек", ((float)summ_time) / CLOCKS_PER_SEC);
	printf("\n");
}
