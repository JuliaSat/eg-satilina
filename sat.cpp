#include "StdAfx.h"
#include "MethodNode.h"

MethodNode::MethodNode(void)
{
	//new action - rep new - new commit
	//its my in master
	name = "";
	body = NULL;
	returnType = NULL;
	isStatic = false;
}


MethodNode::~MethodNode(void)
{
	//comment
}

string MethodNode::descriptor()
{
	//i do first commit in my branch
	string res = "(";

	for(int i = 0; i < arg.size(); i++)
	{
		res += localTable[arg[i]]->type->toStringForDescriptor();;
	}

	res += ")";
	res += returnType->toStringForDescriptor();
	return res;
}

//�������� ����� ������ �� ������� �������, ��� �� ������, ���� ������� ��� ��� ���� ���������
string MethodNode::header()
{
	string res = "";

	//��� ������
	res += parentClass + ".";
	for(int i = 0; i < res.size(); i++)
	{
		if(res[i] == '/')
			res[i] = '.';
	}
	

	//�������� ������
	res += this->name;
	//��������� ������
	res += "(";
	for(int i = 0; i < arg.size(); i++)
	{
		if(outRef[i] == 1)
			res += "ref ";
		else if(outRef[i] == 2)
			res += "out ";

		res += this->localTable[arg[i]]->type->toStringWithArray();

		if(i != arg.size() - 1)
			res += " ";
	}
	res += ")";
	if(res.find_first_of("Classes/") == 0)
	{
		string::iterator it = res.begin();
		res.erase(it, it+8);
	}
	return res;
}

void MethodNode::printTable(FILE* filename)
{
	//local variable
	fprintf(filename, "������� ��������� ����������:\n");
	fprintf(filename, "�;���;���;\n");
	for(map<string, LocalNode*>::iterator i=localTable.begin(); i!=localTable.end(); ++i)
	{
		fprintf(filename, "%s\n", i->second->toStringForPrint().c_str()); 
	}
}

string MethodNode::toStringForPrint()
{
	string str;
	string stat;
	if(this->isStatic)
		stat = "true;";
	else
		stat = "false;";

	string param = "";
	for(int i = 0; i < arg.size(); i++)
	{
		param += arg[i];
		if(i != arg.size() - 1)
			param += ", ";
	}
	param += ";";

	str = this->identificator + string(";") + stat + this->returnType->toStringWithArray() + string(";") +
		this->name + string(";") + this->outRef + string(";") + param;
	return str;
}

void MethodNode::writeClassFile(std::vector<char>* classFile, ClassNode* clss)
{
	 //���������� ����� ������� ACC_PUBLIC (0x0001) � ACC_STATIC (0x0008), ���� ������� Main
		if(isStatic)
			writeU2(9, classFile);
		else
			writeU2(1, classFile);

		// ���������� ����� ��������� ���� CONSTANT_Utf8, ���������� ��� ������ 
		writeU2(clss->findUTF8(name), classFile);

		// ���������� ����� ��������� ���� CONSTANT_Utf8, ���������� ���������� ������
		writeU2(clss->findUTF8(descriptor()), classFile);

		// ���������� ���������� ��������� ������
		writeU2(1, classFile);

		// ���������� ������� ��������� ������
		writeCodeMethodsToClassFile(classFile, clss);
}

void MethodNode::writeCodeMethodsToClassFile(std::vector<char>* classFile, ClassNode* clss)
{
	writeU2(clss->findUTF8("Code"), classFile);

	int lengthAttrPos = classFile->size(); // ������� � ������� ��������� ����� ��������

	// ���������� ������ ����� ��������� ��� ������
	writeU2(1000, classFile); //������ ��������� 1000 ��� �����

	// ���������� ���������� ��������� ���������� ������
	writeU2(localTable.size(), classFile);

	int lengthByteCode = classFile->size(); //������� � ������� ��������� ����� ���� ����

	int withoutByteCode = classFile->size(); //������ ��� ���� ����
		
	generation_stmtList(body, classFile, clss); //����� ��� ���������, ������ ������ �� �����

	int sizeByteCode = classFile->size() - lengthByteCode; //������ ��������

	//������� ������� ���� ����
	writeU4(sizeByteCode, classFile, lengthByteCode);
	
	// ���������� ���������� ������� � ������� ����������
	writeU2(0, classFile);

	// ���������� ���������� ���������
	writeU2(0, classFile);

	int lengthAttr = classFile->size() - lengthAttrPos;
	//������� ����� ��������
	writeU4(lengthAttr, classFile, lengthAttrPos);
}

void MethodNode::generation_stmtList(stmtListStruct* stmtList, std::vector<char>* classFile, ClassNode* clss)
{
	stmtStruct* tmp = NULL;
	if(stmtList != NULL)
	{
		tmp = stmtList->stmtFirst;
	}
	while(tmp != NULL)
	{
		switch(tmp->type)
		{
			case ExprStmt:
				generation_expr(tmp->expr, classFile, clss, false);
				if(tmp->expr->type == New && tmp->expr->newAppeal->type == MethodAppeal)
				{
					classFile->push_back(87);//pop - ���� ��������� ������ ���� � ������, ������ �� �������������
				}
				break;
			case DeclarationStmt:
				break;
			case IfStmt:
				generation_ifStmt(tmp->ifElse, classFile, clss);
				break;
			case WhileStmt:
				if(tmp->whileLoop->type == _while)
					generation_whileStmt(tmp->whileLoop, classFile, clss);
				else
					generation_doWhileStmt(tmp->whileLoop, classFile, clss);
				break;
			case StmtListStmt:
				generation_stmtList(tmp->stmtList, classFile, clss);
				break;
			case ReturnStmt:
				if(tmp->returnExpr == NULL)
					classFile->push_back(177); //������ return
				else
				{
					generation_expr(tmp->returnExpr, classFile, clss);
					if(tmp->returnExpr->typeNode->type == IntType || tmp->returnExpr->typeNode->type == CharType)
						classFile->push_back(172); //ireturn
					else
						classFile->push_back(176); //areturn
				}
				break;
		}
		tmp = tmp->next;
	}
}

void MethodNode::generation_expr(exprStruct* expr, std::vector<char>* classFile, ClassNode* clss, bool isGet, bool inExpr)
{
	string str;
	int pos;
	switch(expr->type)
	{
		case Appeal:
			generation_appeal(expr->appeal, classFile, clss, isGet);
			break;
		case Nulltoken:
			classFile->push_back(1); //aconst_null
			break;
		case UMinus: //���� ������ ��� ������������� �����
			//��������� �� -1
			classFile->push_back(2); //��������� -1
			generation_expr(expr->left, classFile, clss);
			classFile->push_back(104); //imul - ������������� ����� ���������� ���������� �� -1
			break;
		case Not:
			// ������������� ��� ��� ���������
			generation_expr(expr->left, classFile, clss);

			// dup(0x59, 89)
			classFile->push_back(89);

			// ���� ��������� - ������, �� ������� � �������� 0 
			classFile->push_back(154); // ifne(0x9A, 154)

			// �������� �� 0
			writeS2(7, classFile);

			// ��������� 1 � ������ �������
			classFile->push_back(4); // iconst_1

			classFile->push_back(167); // goto
			// �������� ��������
			writeS2(4, classFile);

			// ��������� 0
			classFile->push_back(3); // iconst_0			
			break;
		case Plus: //���� �������� ������ ��� ������������� �����
			generation_expr(expr->left, classFile, clss); //��������� ����� ������� �� ����
			generation_expr(expr->right, classFile, clss); //��������� ������ ������� �� ����
			classFile->push_back(96); //iadd			
			break;
		case Minus: //���� ��������� ������ ��� ������������� �����
			generation_expr(expr->left, classFile, clss); //��������� ����� ������� �� ����
			generation_expr(expr->right, classFile, clss); //��������� ������ ������� �� ����
			classFile->push_back(100); //isub			
			break;
		case Mul: //���� ������ ��� ������������� ��������
			generation_expr(expr->left, classFile, clss); //��������� ����� ������� �� ����
			generation_expr(expr->right, classFile, clss); //��������� ������ ������� �� ����
			classFile->push_back(104); //imul
			break;
		case Div:
			generation_expr(expr->left, classFile, clss); //��������� ����� ������� �� ����
			generation_expr(expr->right, classFile, clss); //��������� ������ ������� �� ����
			classFile->push_back(108); //idiv
			break;
		case Equal:
			generation_expr(expr->left, classFile, clss); //��������� ����� ������� �� ����
			generation_expr(expr->right, classFile, clss); //��������� ������ ������� �� ����
			if(expr->left->typeNode->type == IntType || expr->left->typeNode->type == CharType)
				classFile->push_back(160); //���� �������� �� ����� if_icmpne
			else
				classFile->push_back(166); //if_acmpne
			//�������� ��������
			writeS2(7, classFile);
			classFile->push_back(4); //iconst_1
			classFile->push_back(167); //goto
			//�������� ��������
			writeS2(4, classFile);
			classFile->push_back(3); //iconst_0
			break;
		case NotEqual:
			generation_expr(expr->left, classFile, clss); //��������� ����� ������� �� ����
			generation_expr(expr->right, classFile, clss); //��������� ������ ������� �� ����
			if(expr->left->typeNode->type == IntType || expr->left->typeNode->type == CharType)
				classFile->push_back(159); //���� �������� ����� if_icmpeq
			else
				classFile->push_back(165); //if_acmpeq
			//�������� ��������
			writeS2(7, classFile);
			classFile->push_back(4); //iconst_1
			classFile->push_back(167); //goto
			//�������� ��������
			writeS2(4, classFile);
			classFile->push_back(3); //iconst_0
			break;
		case MoreEqual:
			generation_expr(expr->left, classFile, clss); //��������� ����� ������� �� ����
			generation_expr(expr->right, classFile, clss); //��������� ������ ������� �� ����
			classFile->push_back(161); //���� �������� ������ if_icmplt
			//�������� ��������
			writeS2(7, classFile);
			classFile->push_back(4); //iconst_1
			classFile->push_back(167); //goto
			//�������� ��������
			writeS2(4, classFile);
			classFile->push_back(3); //iconst_0
			break;
		case LessEqual:
			generation_expr(expr->left, classFile, clss); //��������� ����� ������� �� ����
			generation_expr(expr->right, classFile, clss); //��������� ������ ������� �� ����
			classFile->push_back(163); //���� �������� ������ if_icmpgt
			//�������� ��������
			writeS2(7, classFile);
			classFile->push_back(4); //iconst_1
			classFile->push_back(167); //goto
			//�������� ��������
			writeS2(4, classFile);
			classFile->push_back(3); //iconst_0
			break;
		case More:
			generation_expr(expr->left, classFile, clss); //��������� ����� ������� �� ����
			generation_expr(expr->right, classFile, clss); //��������� ������ ������� �� ����
			classFile->push_back(164); //���� �������� <= if_icmple
			//�������� ��������
			writeS2(7, classFile);
			classFile->push_back(4); //iconst_1
			classFile->push_back(167); //goto
			//�������� ��������
			writeS2(4, classFile);
			classFile->push_back(3); //iconst_0
			break;
		case Less:
			generation_expr(expr->left, classFile, clss); //��������� ����� ������� �� ����
			generation_expr(expr->right, classFile, clss); //��������� ������ ������� �� ����
			classFile->push_back(162); //���� �������� >= if_icmpge
			//�������� ��������
			writeS2(7, classFile);
			classFile->push_back(4); //iconst_1
			classFile->push_back(167); //goto
			//�������� ��������
			writeS2(4, classFile);
			classFile->push_back(3); //iconst_0
			break;
		case Or:
			// ���������� ��� ��� ������ ��������
			generation_expr(expr->left, classFile, clss);

			// dup(0x59, 89)
			classFile->push_back(89);

			// ���� ��������� - ������, �� ������� � �������� ������ 
			classFile->push_back(154); // ifne(0x9A, 154)

			pos = classFile->size();

			// ���������� ��� ��� ������� ��������
			generation_expr(expr->right, classFile, clss);

			// ���������� ����������
			classFile->push_back(96); // iadd

			// ���� ����� ����� ������
			classFile->push_back(153); // ifeq(0x99, 153)
			writeS2(7, classFile);

			// ��������� 1 � ������ �������
			classFile->push_back(4); // iconst_1
			classFile->push_back(167); // goto

			// �������� ��������
			writeS2(4, classFile);
			// ��������� 0
			classFile->push_back(3); // iconst_0

			// ���������� �������� � ����������� ����� �������
			writeS2(classFile->size()-pos-2, classFile, pos);
			break;
		case And:
			// ���������� ��� ��� ������ ��������
			generation_expr(expr->left, classFile, clss);

			// dup(0x59, 89)
			classFile->push_back(89);

			// ���� ��������� - ����, �� ������� � �������� 0 
			classFile->push_back(153); // ifeq(0x99, 153)

			pos = classFile->size();

			// ���������� ��� ��� ������� ��������
			generation_expr(expr->right, classFile, clss);

			// ����������� ����������
			classFile->push_back(104); // imul(0x68, 104)

			// ���� ������������ ����� ������
			classFile->push_back(153); // ifeq(0x99, 153)
			writeS2(7, classFile);

			// ��������� 1 � ������ �������
			classFile->push_back(4); // iconst_1
			classFile->push_back(167); // goto

			// �������� ��������
			writeS2(4, classFile);

			// ��������� 0
			classFile->push_back(3); // iconst_0

			// ���������� �������� � ����������� ����� �������
			writeS2(classFile->size()-pos+2, classFile, pos);
			break;
		case Assignment:
			generation_expr(expr->right, classFile, clss); //��������� �������� ������� ���� ���������
			generation_expr(expr->left, classFile, clss, false); //��������� �������� � ���������� ��� ����
			if(inExpr) // ���� ������������ ������������ ��� ������� ��������, �� ����� ����� ���������� ��������� ��������/������ ����������
			{
				if(expr->right->typeNode->type == ObjectType || expr->right->typeNode->type == StringType || expr->right->typeNode->type == AppealType || expr->right->typeNode->isArray == 1)
					classFile->push_back(25); // aload(0x19, 25)
				else
					classFile->push_back(21); // iload(0x15, 21)
				classFile->push_back(expr->left->appeal->varNum);
			}
			break;
		case AssignmentForExprList:
			generation_expr(expr->right, classFile, clss);
			break;
		case New:
			if(expr->newAppeal->isarr == noArr)
			{
				classFile->push_back(187); //new
				writeU2(clss->findClass(expr->typeNode->toString()), classFile); //�������� ��������� ������, ������ �������� �������
				classFile->push_back(89); //dup - ������� ������ �� �����
			}
			generation_appeal(expr->newAppeal, classFile, clss);

			if(expr->newAppeal->isHaveExprList)
			{
				generation_exprList(expr->newAppeal->exprListOfNew, classFile, clss);
			}
			break;
		case CastOfType:
			generation_expr(expr->right, classFile, clss); //��������� �������� �� ����
			if(expr->typeNode->type == IntType) 
			{
				if(expr->right->typeNode->type == ObjectType) //(int)object
				{
					classFile->push_back(192); //checkcast
					writeU2(clss->findClass("java/lang/Integer"), classFile); 

					classFile->push_back(182); //invokevirtual
					writeU2(clss->findMethodref("java/lang/Integer", "intValue", "()I"), classFile);
				}
			}
			else if(expr->typeNode->type == CharType)
			{
				if(expr->right->typeNode->type == ObjectType) //(char)object
				{
					classFile->push_back(192); //checkcast
					writeU2(clss->findClass("java/lang/Character"), classFile); 

					classFile->push_back(182); //invokevirtual
					writeU2(clss->findMethodref("java/lang/Character", "charValue", "()C"), classFile);
				}
				else //(char)int
					classFile->push_back(146); //i2c 
			}
			else if(expr->typeNode->type == ObjectType)
			{
				if(expr->right->typeNode->type == IntType)
				{
					//������� ����������� valueOf
					classFile->push_back(184); //invokestatic
					writeU2(clss->findMethodref("java/lang/Integer","valueOf","(I)Ljava/lang/Integer;"), classFile);
				}
				else if(expr->right->typeNode->type == CharType)
				{
					classFile->push_back(184); //invokestatic
					writeU2(clss->findMethodref("java/lang/Character","valueOf","(C)Ljava/lang/Character;"), classFile);
				}
			}
			else if(expr->typeNode->type == StringType) //� string ����� �������� ������ object
			{
				classFile->push_back(192); //checkcast
				writeU2(clss->findClass("java/lang/String"), classFile); 
			}
			else
			{
				classFile->push_back(192); //checkcast
				writeU2(clss->findClass(expr->typeNode->toString()), classFile);
			}
			break;
		case TernaryArray:
			// ���������� ��� ��� �������
			generation_appeal(expr->ternaryArrayName, classFile, clss);

			// ��������� ��������� ��� �������� ������� � ��������� ������� �� ����
			generation_expr(expr->ternaryArrayIndex, classFile, clss);

			// ���������� ��� ��� ������������� � ������ ��������
			generation_expr(expr->ternaryExpr, classFile, clss);

			// ��������� �������� � ������ 
			// ��� ��������� ����� aastore(0x53, 83)
			if(expr->ternaryExpr->typeNode->type == AppealType || expr->ternaryExpr->typeNode->type == ObjectType || expr->ternaryExpr->typeNode->type == StringType)
				classFile->push_back(83);
			// ��� ��������� ����� iastore (0x4F, 79) 
			else if(expr->ternaryExpr->typeNode->type == IntType)
				classFile->push_back(79);
			// ��� ����� ��� castore (0x55, 85)
			else
				classFile->push_back(85);
			break;
		case TernaryAppeal:
			generation_appeal(expr->ternaryAppeal, classFile, clss); //����������� appeal �� ���� - �������� ������ �� ������ �� ����
			generation_expr(expr->ternaryExpr, classFile, clss); //�������� �������� �� ����
			generation_appeal(expr->ternaryAppealField, classFile, clss, false); //����
			break;
	}
}

void MethodNode::generation_appeal(appealStruct* appeal, std::vector<char>* classFile, ClassNode* clss, bool isGet)
{
	switch(appeal->type)
	{
		case MethodAppeal:
			//invokevirtual, invokespecial
			// ����: ������ � �������� ���������� (���� �����������, �� ������ ��� ���������), ���������
			// �� ���� ����� ����������: ������������ ��������, ���� �� void
			if(appeal->method->invoke == 0 || appeal->method->invoke == 1) 
			{
				// 1. ��������� ������� �� ���� - �� ���� ����� ���� ��� �������, ���� ����������� � new
				// 2. ��������� ��������� �� ����
				generation_factArgList(appeal->method->args, classFile, clss);
				// 3. ��������� �������
				if(appeal->method->invoke == 0)
					classFile->push_back(182); //invokevirtual
				else
					classFile->push_back(183); //invokespecial
				//��� ���������� ������� ����� ����� ��������� methodref
				writeU2(appeal->method->methodref, classFile);
			}
			else
			{
				// 1. ��������� ��������� �� ����
				generation_factArgList(appeal->method->args, classFile, clss);

				classFile->push_back(184);//invokestatic
				//��� ���������� ������� ����� ����� ��������� methodref
				writeU2(appeal->method->methodref, classFile);
			}
			break;
		case ArrayAppeal:
			// iaload (0x2E, 46) � aaload(0x32, 50), caload (0x34, 52)
			// ���� �� ����������: ������ �� ������, ������ ��������
			// ���� �����: �������� �������� �������
			
			// ���������� ��� ��� �������
			generation_appeal(appeal->mass, classFile, clss);

			// ��������� ��������� ��� �������� ������� � ��������� ������� �� ����
			generation_expr(appeal->num, classFile, clss);

			// ��������� ������� ������ ��������
			// ��� ��������� ����� aaload
			if(appeal->typeNode->type == AppealType || appeal->typeNode->type == ObjectType || appeal->typeNode->type == StringType)
				classFile->push_back(50);
			// ��� ����� �aload
			else if(appeal->typeNode->type == CharType)
				classFile->push_back(52);
			// ��� ����� iaload
			else
				classFile->push_back(46);
			break;
		case Int:
			// ��������� ����� �� ����
			// bipush (0x10, 16) ��� ������������
			if(appeal->num_appeal >= -128 && appeal->num_appeal <= 127)
			{
				classFile->push_back(16);
				classFile->push_back(appeal->num_appeal);
			}
			// sipush (0x11, 17) - ������������
			else if(appeal->num_appeal >= -32768 && appeal->num_appeal <= 32767)
			{
				classFile->push_back(17);
				writeS2(appeal->num_appeal, classFile);
			}
			// lcd (0x12, 18) ��� ldc_w (0x13, 19) - �� ���� ���� � ������ (�� ������� ��������)
			else
			{
				if(clss->findInteger(appeal->num_appeal) >= -128 && clss->findInteger(appeal->num_appeal) <= 127)
				{
					classFile->push_back(19);
					classFile->push_back(clss->findInteger(appeal->num_appeal));
				}
				else
				{
					classFile->push_back(19);
					writeU2(clss->findInteger(appeal->num_appeal), classFile);
				}
			}
			break;
		case Charval:
			// ������ 1 ����, ���������� bipush (0x10, 16)
			classFile->push_back(16);
			classFile->push_back(appeal->character_appeal);
			break;
		case Stringval:
			// ���������� lcd (0x12, 18) ��� ldc_w (0x13, 19), ������ ����� ��������� �� ������� ��������
			if(clss->findString(appeal->string_appeal) >= -128 && clss->findString(appeal->string_appeal) <= 127)
			{
				classFile->push_back(18);
				classFile->push_back(clss->findString(appeal->string_appeal));
			}
			else
			{
				classFile->push_back(19);
				writeU2(clss->findString(appeal->string_appeal), classFile);
			}
			break;
		case True:
			// ���������� ��� 1,  iconst_1: 0x4
			classFile->push_back(4);
			break;
		case False:
			// ���������� ��� 0, ���������  iconst_0: 0x3
			classFile->push_back(3);
			break;
		case IdAppeal:
		case GetAppeal:
		case SetAppeal:
			// ���� ��� ����
			if(appeal->isField)
			{
				if(isGet)				
					classFile->push_back(180); // ���������� getfield(0xB4, 180)
				else
					classFile->push_back(181); //putfield
				writeU2(appeal->fieldref, classFile);
				
			}
			// ���� ��� ��������� ����������
			else
			{
				// ���� ��� ������ �������� �� ����������
				if(isGet)
				{
					// ���� ��� ������
					if(appeal->typeNode->type == AppealType || appeal->typeNode->type == ObjectType || appeal->typeNode->type == StringType || appeal->typeNode->isArray == 1)
					{
						// ��������� �� ���� ������ �� ��������� ����������, ��������� aload(0x19, 25)
						classFile->push_back(25);
					}
					// ���� ��� ��������
					else
					{
						// ��������� �� ���� ����� �� ��������� ����������, ��������� iload(0x15, 21)
						classFile->push_back(21);
					}					
				}
				// ���� ��� �������� �������� � ����������
				else
				{
					// ���� ��� ������
					if(appeal->typeNode->type == AppealType || appeal->typeNode->type == ObjectType || appeal->typeNode->type == StringType || appeal->typeNode->isArray == 1)
					{
						// ��������� ������ �� ����� � ��������� ����������, ��������� astore(0x3A, 58)
						classFile->push_back(58);
					}
					// ���� ��� ��������
					else
					{
						// ��������� �������� �� ����� � ��������� ����������, ���������  istore(0x36, 54)
						classFile->push_back(54);
					}
				}
				// ��������� ����� ��������� ����������
				classFile->push_back(appeal->varNum);
			}
			break;
		case BaseAppeal:
		case ThisAppeal:
			// ��������� �� ���� this � �������  aload (0x19, 25)
			classFile->push_back(25);
			classFile->push_back(0);
			break;		
		case NewAppeal:
			classFile->push_back(187); //new
			writeU2(clss->findClass(appeal->typeNode->toString()), classFile); //�������� ��������� ������, ������ �������� �������
			classFile->push_back(89); //dup - ������� ������ �� �����
			break;
		case NewWithExprAppeal:
			generation_createArray(appeal, classFile, clss);
			break;
		case NewWithExprListAppeal:
			generation_createArray(appeal, classFile, clss);

			// �������������� ������ ���������� ����������
			if(appeal->exprListOfNew != NULL)
			{
				exprListStruct * list = appeal->exprListOfNew;
				int i = 0;
				while(list != NULL)
				{
					// ������������ ������ �� ������ � ����� � �������  dup(0x59, 89)
					classFile->push_back(89);

					// ������� ������ ������� � ����
					generation_expr(createAppealExpr(createIntAppeal(i)), classFile, clss);

					// ������� ��������� � ����
					generation_expr(list->expr, classFile, clss);

					// ���� ������ ���������� ���� 
					if(appeal->typeNode->type == StringType ||  appeal->typeNode->type == AppealType || appeal->typeNode->type == ObjectType)
					{
						// ����������  aastore (0x53, 83)
						classFile->push_back(83);
					}
					// ���� ������ �����
					else if(appeal->typeNode->type == CharType)
					{
						// ���������� castore (0x55, 85)
						classFile->push_back(85);
					}
					// ���� ��������� ����
					else
					{
						// ����������  iastore (0x4F, 79)
						classFile->push_back(79);
					}
					i++;
					list = list->next;
				}
			}
			break;
		case Point:
			generation_appeal(appeal->left,classFile, clss);
			generation_appeal(appeal->right, classFile, clss, isGet);
			break;
	}
}

void MethodNode::generation_createArray(appealStruct* appeal, std::vector<char>* classFile, ClassNode* clss)
{
	// ��������� ������ �� ����
	generation_expr(appeal->exprOfNew, classFile, clss);

	// ���� ������ ��������� �����
	if(appeal->typeNode->type == StringType || appeal->typeNode->type == ObjectType || appeal->typeNode->type == AppealType)
	{
		// ������� ������ � ������� ������� anewarray (0xBD, 189)
		classFile->push_back(189);

		// ���������� ��� ��������� �������, ���� ��� �� ������� ��������
		writeU2(clss->findClass(appeal->typeOfNew->toStringForDescriptor()), classFile);
	}
	// ���� ������ �������� �����
	else
	{
		// ������� ������ � ������� ������� newarray (0xBC, 188)
		classFile->push_back(188);

		// ���������� ��� ��������� �������
		if(appeal->typeOfNew->type == IntType)
			classFile->push_back(10); // T_INT 10 
		else if(appeal->typeOfNew->type == CharType)
			classFile->push_back(5); //T_CHAR 5
	}
}

void MethodNode::generation_factArgList(factArgListStruct* factArgs, std::vector<char>* classFile, ClassNode* clss)
{
	while(factArgs != NULL)
	{
		generation_expr(factArgs->expr, classFile, clss);
		factArgs = factArgs->next;
	}
}

void MethodNode::generation_ifStmt(ifElseStruct * ifElse, std::vector<char>* classFile, ClassNode* clss)
{	
	ifElseStruct * tmp = ifElse;
	stmtStruct * elseStmt = NULL;

	// ������� ������ ��������
	vector <vector<char>> codeOfBranches;
	vector <vector<char>> codeOfConditions;
	vector<char> tmpCode;
	vector<char> tmpCondition;
	while(tmp != NULL)
	{
		// �������� �� ���� ������ � ��� ������ ���������� ���
		if(tmp->body != NULL)
			generation_stmtList(tmp->body->stmtList, &tmpCode, clss);
		generation_expr(tmp->condition, &tmpCondition, clss);

		codeOfBranches.push_back(tmpCode);
		codeOfConditions.push_back(tmpCondition);
		tmpCode.clear();
		tmpCondition.clear();

		if(tmp->nextBranch != NULL)
		{
			if(tmp->nextBranch->type == IfStmt)
				tmp = tmp->nextBranch->ifElse;
			else
			{
				elseStmt = tmp->nextBranch;
				break;
			}
		}
		else
			break;
	}
	if(elseStmt != NULL)
	{
		generation_stmtList(elseStmt->stmtList, &tmpCode, clss);
		codeOfBranches.push_back(tmpCode);
	}
	int count = codeOfBranches.size();

	int stepToEnd = 0;

	for(int i = count-1; i >= 0; i--)
	{
		if(elseStmt == NULL || i != codeOfBranches.size()-1)
		{
			// � ����� ������ ����� ��������� ����������� ������� �� ����� �������� (����� ��������� �����) �������� goto(0xA7, 167)
			if(i+1 != codeOfBranches.size())
			{
				codeOfBranches[i].push_back(167);
				writeS2(stepToEnd, &codeOfBranches[i]);
				stepToEnd += 1;
			}

			// � ������ ������ ����� ��������� ��� ��� ������� � ������� �� ������ ��������� �����, ���� ������� �� ������� (����� else) �������� ifeq(0x99, 153)
			codeOfConditions[i].push_back(153);
			writeS2(codeOfBranches[i].size()+3, &codeOfConditions[i]);
			stepToEnd += 3;		
		
			// ��������� � ���� ������� ��� ���� �����
			codeOfConditions[i].insert(codeOfConditions[i].end(), codeOfBranches[i].begin(), codeOfBranches[i].end());
		}

		if(elseStmt != NULL)
			stepToEnd += codeOfBranches[i].size()+2;
		if(elseStmt == NULL)
			stepToEnd += codeOfConditions[i].size();
	}

	// �������� �� ���� �������� � ��������� ��� ������ ����� � ��������� ����
	for(int i = 0; i < codeOfConditions.size(); i++)
	{
		classFile->insert(classFile->end(), codeOfConditions[i].begin(), codeOfConditions[i].end());
	}
	if(elseStmt != NULL)
		classFile->insert(classFile->end(), codeOfBranches[codeOfBranches.size()-1].begin(), codeOfBranches[codeOfBranches.size()-1].end());
}

void MethodNode::generation_whileStmt(whileStruct * loop, std::vector<char>* classFile, ClassNode* clss)
{
	vector <char> bodyCode;
	vector <char> conditionCode;
	// ���������� ��� ��� �������
	if(!loop->fromForeach)
	{
		generation_expr(loop->condition, &conditionCode, clss);
	}
	else
	{
		// ��� ������ ������ ������� �������� �������� ������ ������� ������� ($counter < $arr.length())
		generation_expr(loop->condition->left, &conditionCode, clss); //��������� ����� ������� �� ����
		
		// ��������� �� ���� ������ �� ��������� ����������, ��������� aload(0x19, 25)
		conditionCode.push_back(25);
		// ��������� ����� ��������� ����������
		conditionCode.push_back(loop->condition->right->appeal->varNum);
		// arraylength (0xBE, 190)
		conditionCode.push_back(190);

		conditionCode.push_back(162); //���� �������� >= if_icmpge
		//�������� ��������
		writeS2(7, &conditionCode);
		conditionCode.push_back(4); //iconst_1
		conditionCode.push_back(167); //goto
		//�������� ��������
		writeS2(4, &conditionCode);
		conditionCode.push_back(3); //iconst_0
	}

	// ���������� ��� ��� ����
	generation_stmtList(loop->body->stmtList, &bodyCode, clss);

	// ���������� �������� ���������� ������� � ������� �� ������� ����� ifeq(0x99, 153)
	conditionCode.push_back(153);
	writeS2(bodyCode.size()+6, &conditionCode);

	// � ����� ���� ����� ���������� ����������� ������� �� ������� ����� � ������� goto(0xA7, 167)
	bodyCode.push_back(167);
	writeS2(-bodyCode.size()-conditionCode.size(), &bodyCode);

	// �������� � ����� ������� � ���� �����
	classFile->insert(classFile->end(), conditionCode.begin(), conditionCode.end());
	classFile->insert(classFile->end(), bodyCode.begin(), bodyCode.end());
}

void MethodNode::generation_doWhileStmt(whileStruct * loop, std::vector<char>* classFile, ClassNode* clss)
{
	vector <char> bodyCode;

	// ���������� ��� ��� ���� �����
	generation_stmtList(loop->body->stmtList, &bodyCode, clss);

	// ���������� ��� ��� ������� �����
	generation_expr(loop->condition, &bodyCode, clss);

	// � ����� ������� ��������� �������� ���������� � ������� �� ������ ����� ifne(0x9A, 154)
	bodyCode.push_back(154);
	writeS2(-bodyCode.size(), &bodyCode);

	// �������� ��� � ����� ����
	classFile->insert(classFile->end(), bodyCode.begin(), bodyCode.end());
}

void MethodNode::generation_exprList(exprListStruct* exprList, std::vector<char>* classFile, ClassNode* clss)
{
	exprListStruct* tmp = exprList;
	int count = 0;
	while(tmp != NULL)
	{
		//��������� ���������� ������ � �����

		tmp = tmp->next;
	}
}

void MethodNode::generation_exprForList(exprListStruct* exprList, std::vector<char>* classFile, ClassNode* clss)
{

}